<?php
	session_start();
	require_once('./php/functions.php');
	if(empty($_SESSION['id_usuario'])) header('Location:'.URL_SITE);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Reserva</title>
	<script type='text/javascript' src='./js/miajax.js'></script>
	<link type="text/css" rel="stylesheet" href="./css/estilo.css" />
</head>
<body>
	<h1>Reserva</h1>
	<div id="asientos"></div>
</body>
<script type="text/javascript">	
	window.onload = refresca();
</script>
</html>